create table if not exists todo (
    id         bigint auto_increment primary key,
    done      boolean       null,
    text       varchar(255) null
);