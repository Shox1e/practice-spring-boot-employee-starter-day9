package com.afs.restapi.controller;

import com.afs.restapi.entity.Todo;
import com.afs.restapi.service.ToDoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    @Autowired
    private ToDoService toDoService;
    @GetMapping
    public List<Todo> AllTodo(){
        return toDoService.findAllTodo();
    }

    @GetMapping("/{id}")
    public Todo TodoById(@PathVariable Long id){
        return toDoService.findById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Todo addTodo(@RequestBody Todo todo){
        return toDoService.create(todo);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void Todo(@PathVariable Long id){
        toDoService.remove(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Todo updateTodo(@PathVariable Long id,@RequestBody Todo todo) {
        return toDoService.updateTodo(id, todo);
    }
}
