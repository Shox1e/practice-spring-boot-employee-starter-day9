package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.Dto.CompanyResponse;
import com.afs.restapi.service.Mapper.CompanyMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final InMemoryCompanyRepository inMemoryCompanyRepository;
    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;
    private JPAEmployeeRepository jpaEmployeeRepository;
    private JPACompanyRepository jpaCompanyRepository;

    public CompanyService(InMemoryCompanyRepository inMemoryCompanyRepository, InMemoryEmployeeRepository inMemoryEmployeeRepository, JPAEmployeeRepository jpaEmployeeRepository, JPACompanyRepository jpaCompanyRepository) {
        this.inMemoryCompanyRepository = inMemoryCompanyRepository;
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
        this.jpaCompanyRepository = jpaCompanyRepository;
    }

    public InMemoryCompanyRepository getCompanyRepository() {
        return inMemoryCompanyRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<CompanyResponse> findAll() {
        return jpaCompanyRepository.findAll().stream().map(CompanyMapper::toResponse).collect(Collectors.toList());
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return jpaCompanyRepository.findAll(PageRequest.of(page-1, size)).toList();
    }

    public CompanyResponse findById(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        CompanyResponse companyResponse = CompanyMapper.toResponse(company);
        companyResponse.setEmployeeCount(company.getEmployees().size());
        return companyResponse;
    }

    public CompanyResponse update(Long id, Company company) {
        Optional<Company> optionalCompany = jpaCompanyRepository.findById(id);
        optionalCompany.ifPresent(previousCompany -> previousCompany.setName(company.getName()));
        jpaCompanyRepository.save(optionalCompany.get());
        return CompanyMapper.toResponse(optionalCompany.get());
    }

    public CompanyResponse create(Company company) {
        return CompanyMapper.toResponse(jpaCompanyRepository.save(company));
    }

    public List<Employee> findEmployeesByCompanyId(Long id) {
        return jpaEmployeeRepository.findByCompanyId(id);
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
