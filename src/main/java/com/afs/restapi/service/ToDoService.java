package com.afs.restapi.service;

import com.afs.restapi.entity.Todo;
import com.afs.restapi.exception.TodoNotFoundExcepiton;
import com.afs.restapi.repository.JPAToDoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


@Service
public class ToDoService {
    @Autowired
    private JPAToDoRepository jpaToDoRepository;


    public List<Todo> findAllTodo(){
        return new ArrayList<>(jpaToDoRepository.findAll());
    }

    public Todo create(Todo todo){
        jpaToDoRepository.save(todo);
        return  todo;
    }

    public void remove(Long id){
        jpaToDoRepository.deleteById(id);
    }

    public Todo updateTodo(Long id,Todo todo){
        Todo updatetodo = this.findById(id);
        if (!Objects.isNull(todo.getDone())){
            updatetodo.setDone(todo.getDone());
        }
        if (!Objects.isNull(todo.getText())){
            updatetodo.setText(todo.getText());
        }
        return jpaToDoRepository.save(updatetodo);
    }

    public Todo findById(Long id){
        return jpaToDoRepository.findById(id).orElseThrow(TodoNotFoundExcepiton::new);
    }
}
