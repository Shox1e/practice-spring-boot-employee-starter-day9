package com.afs.restapi.service.Mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.Dto.CompanyRequest;
import com.afs.restapi.service.Dto.CompanyResponse;
import com.afs.restapi.service.Dto.EmployeeRequest;
import com.afs.restapi.service.Dto.EmployeeResponse;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {
    public static Company toEntity(CompanyRequest companyRequest) {
        Company company = new Company();
        BeanUtils.copyProperties(companyRequest,company);
        return company;
    }

    public static CompanyRequest toRequest(Company company) {
        CompanyRequest companyRequest = new CompanyRequest();
        BeanUtils.copyProperties(company,companyRequest);
        return companyRequest;
    }

    public static CompanyResponse toResponse(Company company) {
        CompanyResponse companyResponse = new CompanyResponse();
        BeanUtils.copyProperties(company,companyResponse);
        return companyResponse;
    }
}
