package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.Dto.EmployeeRequest;
import com.afs.restapi.service.Dto.EmployeeResponse;
import com.afs.restapi.service.Mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private JPAEmployeeRepository jpaEmployeeRepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return jpaEmployeeRepository.findAll().stream().map(EmployeeMapper::toResponse).collect(Collectors.toList());
    }

    public EmployeeResponse findById(Long id) {
        return EmployeeMapper.toResponse(jpaEmployeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new));
    }

    public Employee findByEmployeeId(Long id) {
        return jpaEmployeeRepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public EmployeeResponse update(Long id, EmployeeRequest employeeRequest) {
        Employee toBeUpdatedEmployee = findByEmployeeId(id);
        if (employeeRequest.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employeeRequest.getSalary());
        }
        if (employeeRequest.getAge() != null) {
            toBeUpdatedEmployee.setAge(employeeRequest.getAge());
        }
        jpaEmployeeRepository.save(toBeUpdatedEmployee);
        return EmployeeMapper.toResponse(toBeUpdatedEmployee);
    }

    public List<Employee> findAllByGender(String gender) {
        return jpaEmployeeRepository.findByGender(gender);
    }

    public EmployeeResponse create(EmployeeRequest employeeRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeRequest);
        jpaEmployeeRepository.save(employee);
        return EmployeeMapper.toResponse(employee);

    }

    public List<Employee> findByPage(Integer page, Integer size) {
        return jpaEmployeeRepository.findAll(PageRequest.of(page-1,size)).toList();
    }

    public void delete(Long id) {
        jpaEmployeeRepository.deleteById(id);
    }
}
