package com.afs.restapi.entity;

import javax.persistence.*;

@Entity
@Table(name = "todo")
public class Todo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String text;
    @Column
    private Boolean done;

    public Todo(Long id, String text, Boolean done) {
        this.id = id;
        this.text = text;
        this.done = done;
    }

    public Todo(){

    }

    public Todo(String text, Boolean done) {
        this.text = text;
        this.done = done;
    }

    public Todo(String text) {
        this.text = text;
    }

    public Todo(boolean done) {
        this.done=done;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
