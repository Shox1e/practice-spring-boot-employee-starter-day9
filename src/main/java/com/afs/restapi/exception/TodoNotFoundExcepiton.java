package com.afs.restapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TodoNotFoundExcepiton extends RuntimeException{
    public TodoNotFoundExcepiton() {
        super("todo not found");
    }
}
