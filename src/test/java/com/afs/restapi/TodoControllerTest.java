package com.afs.restapi;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.entity.Todo;
import com.afs.restapi.repository.JPAToDoRepository;
import com.afs.restapi.service.Dto.CompanyRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private JPAToDoRepository jpaToDoRepository;

    @BeforeEach
    void setUp() {
        jpaToDoRepository.deleteAll();
    }

    @Test
    void should_create_todo() throws Exception {
        Todo todo = new Todo("123",false);
        ObjectMapper objectMapper = new ObjectMapper();
        String request = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todo")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(request))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo = new Todo("123",false);
        Todo savetodo = jpaToDoRepository.save(todo);

        mockMvc.perform(delete("/todo/{id}", savetodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertFalse(jpaToDoRepository.findById(savetodo.getId()).isPresent());
    }


    @Test
    void should_update_todo_text() throws Exception {
        Todo todo = new Todo("123",false);
        Todo savetodo = jpaToDoRepository.save(todo);


        ObjectMapper objectMapper = new ObjectMapper();
        Todo updatetodo = new Todo("456");
        String companyUpdateRequestJson = objectMapper.writeValueAsString(updatetodo);
        mockMvc.perform(put("/todo/{id}", savetodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyUpdateRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Todo finaltodo = jpaToDoRepository.findById(savetodo.getId()).get();

        Assertions.assertEquals(finaltodo.getId(), savetodo.getId());
        Assertions.assertNotEquals(savetodo.getText(), finaltodo.getText());
    }


    @Test
    void should_update_todo_done() throws Exception {
        Todo todo = new Todo("123",false);
        Todo savetodo = jpaToDoRepository.save(todo);


        ObjectMapper objectMapper = new ObjectMapper();
        Todo updatetodo = new Todo(true);
        String companyUpdateRequestJson = objectMapper.writeValueAsString(updatetodo);
        mockMvc.perform(put("/todo/{id}", savetodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(companyUpdateRequestJson))
                .andExpect(MockMvcResultMatchers.status().is(204));

        Todo finaltodo = jpaToDoRepository.findById(savetodo.getId()).get();

        Assertions.assertEquals(finaltodo.getId(), savetodo.getId());
        Assertions.assertNotEquals(savetodo.getDone(), finaltodo.getDone());
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo1 = new Todo("123",false);
        jpaToDoRepository.save(todo1);

        Todo todo2 = new Todo("456",false);
        jpaToDoRepository.save(todo2);

        mockMvc.perform(get("/todo"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(todo1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].text").value(todo1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].done").value(todo1.getDone()));
    }

    @Test
    void should_find_todosById() throws Exception {
        Todo todo1 = new Todo("123",false);
        jpaToDoRepository.save(todo1);

        Todo todo2 = new Todo("456",false);
        jpaToDoRepository.save(todo2);

        mockMvc.perform(get("/todo/{id}",todo1.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(todo1.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value(todo1.getText()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo1.getDone()));
    }

    @Test
    void should_throw_NullObjException_when_get_by_id_given_id() throws Exception {
        mockMvc.perform(get("/todo/100"))
                .andExpect(MockMvcResultMatchers.status().is(404))
                .andExpect(MockMvcResultMatchers.jsonPath("$.message").value("todo not found"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.code").value("404"));
    }
}
