O：Today, I reviewed the three-layer architecture of Spring Boot and learned how to use JPA to operate databases. At the same time, I created a testing environment and conducted tests in the testing environment instead of directly operating the database
R：I feel very happy about today's class and can make progress faster. 
I：What impressed me the most was today's learning about JPA, which is a bit similar to the Mybatis I have learned. The difference is the use of annotations. Through my previous learning of Mybatis, I can also get started with JPA faster
D：Today, I mainly learned about JPA, how to connect to a database, and how to use APIs to quickly operate the database instead of writing SQL statements, which improved the efficiency of code development
